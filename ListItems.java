package com.example.marek.volleyexample;


import org.parceler.Parcel;

/**
 * Created by Marek on 2016-05-31.
 */
@Parcel
public class ListItems {

    String title;
    String longitude;
    String latitude;
    String rowIcon;

    public ListItems(){

    }

    public ListItems(String latitude, String longitude, String title) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
    }

    public ListItems(String latitude, String longitude, String rowIcon, String title) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.rowIcon = rowIcon;
        this.title = title;
    }


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getRowIcon() {
        return rowIcon;
    }

    public void setRowIcon(String rowIcon) {
        this.rowIcon = rowIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }







}
