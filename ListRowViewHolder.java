package com.example.marek.volleyexample;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

/**
 * Created by Marek on 2016-05-31.
 */
public class ListRowViewHolder extends RecyclerView.ViewHolder {

    protected NetworkImageView rowIcon;
    protected TextView title;
    protected TextView latitude;
    protected TextView longitude;
    protected RelativeLayout relativeLayout;


    public ListRowViewHolder(View view) {
        super(view);

        this.rowIcon = (NetworkImageView) view.findViewById(R.id.row_icon);
        this.title = (TextView) view.findViewById(R.id.title);
        this.latitude = (TextView) view.findViewById(R.id.latitode);
        this.longitude = (TextView) view.findViewById(R.id.longitude);
        this.relativeLayout = (RelativeLayout) view.findViewById(R.id.row_layout);
        view.setClickable(true);
    }
}
