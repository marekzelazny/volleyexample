package com.example.marek.volleyexample;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final String TAG = "Sparsowałem to: ";
    private static final String url = "http://konkursoowo.pl/API/contest.php";
    private List<ListItems> listItemsList = new ArrayList<>();
    private List<LatLng> coords = new ArrayList<>();
    private GoogleMap mMap;
    private MarkerOptions options;
    private RotateAnimation rotate;
    private ImageButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        fab = (ImageButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rotate = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotate.setDuration(300);
                rotate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {}

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Intent intent = new Intent(MainActivity.this,MarkerViewer.class);
                        intent.putExtra("list", Parcels.wrap(listItemsList));
                        startActivity(intent);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {}
                });
                fab.startAnimation(rotate);

            }
        });

        updateList(url);
        createCoords();
    }

    public void updateList(String url){

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d(TAG, response.toString());

                        try {
                            JSONArray jsonArray = response.getJSONArray("contests");

                            for(int i = 0; i<jsonArray.length(); i++) {

                                JSONObject marker = jsonArray.getJSONObject(i);

                                String title = marker.getString("id");
                                String lat = marker.getString("latitude");
                                String lng = marker.getString("longitude");
                                String rowIcon ="http://www.iconsdb.com/icons/download/orange/map-marker-2-128.png";

                                ListItems listItems = new ListItems(lat, lng, rowIcon, title);
                                listItemsList.add(listItems);

                            }




                        }catch (JSONException e) {
                            Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }


                    }
                },

                new Response.ErrorListener(){

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        VolleyLog.d("Błąd: ", error.getMessage());

                    }
        });



        queue.add(jsonObjectRequest);

    }

    public void createCoords(){

        for(int i=0; i<listItemsList.size(); i++){

            LatLng coord = new LatLng(Double.valueOf(listItemsList.get(i).getLatitude()),
                                    Double.valueOf(listItemsList.get(i).getLongitude()));
            coords.add(coord);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        options = new MarkerOptions();
        ListItems clickedMarker = Parcels.unwrap(getIntent().getParcelableExtra("marker"));


        try {
            mMap.setMyLocationEnabled(true);
        } catch (SecurityException a){
            Toast.makeText(getApplicationContext(),"Brak wymaganych uprawnień", Toast.LENGTH_LONG).show();
        }

        for(int i=0; i<coords.size(); i++) {



            String title = listItemsList.get(i).getTitle();

            mMap.addMarker(new MarkerOptions().position(coords.get(i)).title(title)).showInfoWindow();
//            options.position(coords.get(i));
//            options.title(title)
//                    .snippet("opis");
//            mMap.addMarker(options).showInfoWindow();

        }

        if(clickedMarker != null){

            double dLat = Double.parseDouble(clickedMarker.getLatitude());
            double dLng = Double.parseDouble(clickedMarker.getLongitude());

            LatLng focunOn = new LatLng(dLat, dLng);
            CameraPosition cameraPosition= new CameraPosition.Builder().target(focunOn).zoom(16).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        } else {

//            CameraPosition cameraPosition = new CameraPosition.Builder().target(coords.get(0)).zoom(16).build();
//            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }




    }
}
