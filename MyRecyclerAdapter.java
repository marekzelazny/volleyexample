package com.example.marek.volleyexample;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;

import org.parceler.Parcels;

import java.util.List;

/**
 * Created by Marek on 2016-06-01.
 */
public class MyRecyclerAdapter extends RecyclerView.Adapter<ListRowViewHolder> {

    private List<ListItems> listItemsList;
    private ListItems listItems;
    private Context mContext;
    private ImageLoader mImageLoader;
    private int focusedItem = 0;

    public MyRecyclerAdapter(Context context, List<ListItems> listItemsList){
        this.mContext = context;
        this.listItemsList = listItemsList;
    }

    @Override
    public ListRowViewHolder onCreateViewHolder(final ViewGroup viewGroup, int position){
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_row, null);
        ListRowViewHolder holder = new ListRowViewHolder(v);

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                TextView name = (TextView) v.findViewById(R.id.title);
                TextView lat = (TextView) v.findViewById(R.id.latitode);
                TextView lng = (TextView) v.findViewById(R.id.longitude);

                String sName = name.getText().toString();
                String sLat = lat.getText().toString();
                String sLng = lng.getText().toString();

                listItems = new ListItems(sLat, sLng, sName);

                Intent intent = new Intent(mContext, MainActivity.class);
                intent.putExtra("marker", Parcels.wrap(listItems));
                mContext.startActivity(intent);

            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(final ListRowViewHolder listRowViewHolder, int position){
        ListItems listItems = listItemsList.get(position);
        listRowViewHolder.itemView.setSelected(focusedItem == position);

        listRowViewHolder.getLayoutPosition();

        mImageLoader = MySingleton.getInstance(mContext).getImageLoader();

        listRowViewHolder.rowIcon.setImageUrl(listItems.getRowIcon(), mImageLoader);
        listRowViewHolder.rowIcon.setDefaultImageResId(R.drawable.row_icon_placeholder);

        listRowViewHolder.title.setText(Html.fromHtml(listItems.getTitle()));
        listRowViewHolder.latitude.setText(Html.fromHtml(listItems.getLatitude()));
        listRowViewHolder.longitude.setText(Html.fromHtml(listItems.getLongitude()));
    }

    public void clearAdapter(){
        listItemsList.clear();
        notifyDataSetChanged();
    }

    public int getItemCount(){
        return (null != listItemsList ? listItemsList.size() : 0);
    }

}
